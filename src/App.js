import logo from "./logo.png";
import "./App.css";

import Table from "./table";

import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";

import { makeStyles } from "@material-ui/core/styles";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";

import HomeIcon from "@material-ui/icons/Home";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Avatar from "@material-ui/core/Avatar";
import FavoriteIcon from "@material-ui/icons/Favorite";
import AssessmentOutlinedIcon from "@material-ui/icons/AssessmentOutlined";
import PrintOutlinedIcon from "@material-ui/icons/PrintOutlined";
import SystemUpdateAltOutlinedIcon from "@material-ui/icons/SystemUpdateAltOutlined";

const useStyles = makeStyles((theme) => ({
  link: {
    display: "flex",
    fontWeight: "bold",
  },
  icon: {
    marginRight: theme.spacing(0.5),
    width: 20,
    height: 20,
  },
  hilight: {
    color: "#ffa640",
    fontWeight: "bold",
    fontSize: "22px",
  },
  hilight2: {
    color: "#ffa640",
    fontWeight: "bold",
    fontSize: "18px",
  },
  hilight3: {
    color: "white",
    fontSize: "14px",
  },
  headerCommand: {
    display: "flex",
  },
  headerCommandColor: {
    backgroundColor: "#ffa640",
    marginLeft: "6px",
  },
  headerCommandColor2: {
    backgroundColor: "#ffa640",
    marginLeft: "6px",
    opacity: "0.5",
  },
}));

function handleClick(event) {
  event.preventDefault();
  console.info("You clicked a breadcrumb.");
}

function App() {
  const classes = useStyles();

  return (
    <div className="App">
      <Box display="flex" height="46px">
        <Box p={1} width="70%" bgcolor="black">
          <img src={logo} className="App-logo" alt="logo" />
        </Box>
        <Box p={1} bgcolor="black">
          <AccountCircleIcon style={{ color: "white", fontSize: "35px" }} />
        </Box>
        <Box p={1} flexShrink={0} bgcolor="black">
          <Typography style={{ color: "#ffa640", fontSize: "12px" }}>
            MR.ADMIN
          </Typography>
          <Typography style={{ color: "white", fontSize: "12px" }}>
            Head Quarter
          </Typography>
        </Box>
        <Box
          pt={1} pr={2}
          width="30%"
          display="flex"
          bgcolor="#ffa640"
          justifyContent="flex-end"
        >
          <Typography style={{ color: "white" }}>Menu</Typography>
          <FavoriteIcon style={{ color: "white" }} />
        </Box>
      </Box>

      <Box display="flex" p={1}>
        <Box width="70%" p={1} display="flex">
          <Breadcrumbs aria-label="breadcrumb">
            <Link
              color="inherit"
              href="/"
              onClick={handleClick}
              className={classes.link}
            >
              <HomeIcon className={classes.icon} />
            </Link>
            <Link
              color="inherit"
              href="/getting-started/installation/"
              onClick={handleClick}
              className={classes.link}
            >
              Business Insight
            </Link>
            <Link
              color="inherit"
              href="/getting-started/installation/"
              onClick={handleClick}
              className={classes.link}
            >
              Report
            </Link>
            <Link
              color="inherit"
              href="/getting-started/installation/"
              onClick={handleClick}
              className={classes.link}
            >
              Member
            </Link>
            <Typography className={classes.hilight}>Member</Typography>
          </Breadcrumbs>
        </Box>
        <Box width="30%" p={1} display="flex" justifyContent="flex-end">
          <Avatar className={classes.headerCommandColor}>
            <AssessmentOutlinedIcon />
          </Avatar>
          <Avatar className={classes.headerCommandColor2}>
            <SystemUpdateAltOutlinedIcon />
          </Avatar>
          <Avatar className={classes.headerCommandColor2}>
            <PrintOutlinedIcon />
          </Avatar>
        </Box>
      </Box>

      <Box display="flex" p={1}>
        <Box width="70%" p={1} display="flex">
          <Typography className={classes.hilight2}>
            Yearly Member 01-Jan-2020 to 31-Dec-2020
          </Typography>
        </Box>
        <Box width="30%" p={1} display="flex" justifyContent="flex-end"></Box>
      </Box>

      <Grid container>
        <Grid item container sm={4}>
          <Grid item sm={10}>
            <Box
              bgcolor="warning.main"
              color="background.paper"
              p={2} ml={2}
              display="flex"
              fontSize="24px"
            >
              Total Members:
            </Box>
          </Grid>
          <Grid item sm={2}>
            <Box
              bgcolor="warning.main"
              color="background.paper"
              p={2}
              display="flex"
              justifyContent="flex-end"
              fontSize="24px"
            >
              5
            </Box>
          </Grid>
        </Grid>
        <Grid item sm={8}>
          <Grid item style={{ textAlign: "center"}}>
            <Box bgcolor="text.secondary" color="background.paper" p={1} fontSize="24px"
            style={{marginRight: "16px"}}>
              ttt
            </Box>
          </Grid>
        </Grid>

        <Grid item container sm={4}>
          <Grid item sm={10}>
            <Box bgcolor="warning.main" color="background.paper" p={2} ml={2} fontSize="26px">
              Total Rev.(THB):
            </Box>
          </Grid>
          <Grid item sm={2}>
            <Box
              bgcolor="warning.main"
              color="background.paper"
              p={2}
              display="flex"
              justifyContent="flex-end"
              fontSize="26px"
            >
              639K
            </Box>
          </Grid>
        </Grid>

        <Grid item container sm={8}>
          <Grid item sm={6}>
            <Box bgcolor="text.secondary" color="background.paper" p={1} mt={-2}
              fontSize="19px">
              Total Members:
            </Box>
          </Grid>
          <Grid item sm={6}>
            <Box
              bgcolor="text.secondary"
              color="background.paper"
              p={1} mr={2} mt={-2}
              display="flex"
              justifyContent="flex-end"
              fontSize="19px"
            >
              5
            </Box>
          </Grid>

          <Grid item sm={6}>
            <Box bgcolor="text.secondary" color="background.paper" p={1} pb={1}
              fontSize="18px">
              Total Rev.(THB):
            </Box>
          </Grid>
          <Grid item sm={6}>
            <Box
              bgcolor="text.secondary"
              color="background.paper"
              p={1} mr={2}
              display="flex"
              justifyContent="flex-end"
              fontSize="18px"
            >
              639K
            </Box>
          </Grid>
        </Grid>
      </Grid>

      <Box m={2}>
        <Table />
      </Box>
      
      {/* <header className="App-header"></header> */}
    </div>
  );
}

export default App;
